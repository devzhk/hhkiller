package net.hhkiller.server;

import com.google.gson.Gson;
import net.hhkiller.request.*;
import net.hhkiller.response.EmptyResponse;
import net.hhkiller.response.ErrorResponse;
import net.hhkiller.response.LoginUserDtoResponse;
import net.hhkiller.model.Requirement;
import net.hhkiller.model.Skill;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Comparator;
import java.util.Set;
import java.util.TreeSet;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class TestServer {
    private static Server server;
    private static Gson gson;
    private static Set<Requirement> requirements;
    private static Set<SkillDto> skills;

    @BeforeClass
    public static void precondition() {
        gson = new Gson();
        server = new Server();
        requirements = new TreeSet<>(Comparator.comparing(Skill::getName));
        skills = new TreeSet<>(Comparator.comparing(SkillDto::getName));
        server.startServer(null);
    }

    @Test
    public void loginAlreadyLoggedUser() {
        RegisterEmployeeDtoRequest request = new RegisterEmployeeDtoRequest("admin@mail.ru", "newadmin1", "1qa@WS3ed", "Ivan", "Pupkin", null, true);
        String jsonRequest = gson.toJson(request);
        String jsonResponse = server.registerEmployee(jsonRequest);
        LoginUserDtoResponse token = gson.fromJson(jsonResponse, LoginUserDtoResponse.class);
        assertNotNull(token.getToken());

        LoginUserDtoRequest request1 = new LoginUserDtoRequest("newadmin1", "1qa@WS3ed");
        String jsonRequest1 = gson.toJson(request1);
        String jsonResponse1 = server.login(jsonRequest1);
        assertEquals(gson.toJson(new ErrorResponse("Пользователь уже залогинен")), jsonResponse1);
    }

    @Test
    public void successLoginUser() {
        RegisterEmployeeDtoRequest request = new RegisterEmployeeDtoRequest("admin@mail.ru", "newadmin2", "1qa@WS3ed", "Ivan", "Pupkin", null, true);
        String jsonRequest = gson.toJson(request);
        String jsonResponse = server.registerEmployee(jsonRequest);
        LoginUserDtoResponse token = gson.fromJson(jsonResponse, LoginUserDtoResponse.class);
        assertNotNull(token.getToken());

        String status = server.logout(jsonResponse);
        assertEquals(gson.toJson(new EmptyResponse()), status);

        LoginUserDtoRequest request1 = new LoginUserDtoRequest("newadmin2", "1qa@WS3ed");
        String jsonRequest1 = gson.toJson(request1);
        String jsonResponse1 = server.login(jsonRequest1);
        LoginUserDtoResponse token1 = gson.fromJson(jsonResponse1, LoginUserDtoResponse.class);
        assertNotNull(token1.getToken());
    }

    @Test
    public void successLogoutUser() {
        RegisterEmployeeDtoRequest request = new RegisterEmployeeDtoRequest("admin@mail.ru", "newaddmin2", "1qa@WS3ed", "Ivan", "Pupkin", null, true);
        String jsonRequest = gson.toJson(request);
        String jsonResponse = server.registerEmployee(jsonRequest);
        LoginUserDtoResponse token = gson.fromJson(jsonResponse, LoginUserDtoResponse.class);
        assertNotNull(token.getToken());

        String status = server.logout(jsonResponse);
        assertEquals(gson.toJson(new EmptyResponse()), status);
    }

    @Test
    public void deleteAuthorizedUser() {
        RegisterEmployeeDtoRequest request = new RegisterEmployeeDtoRequest("admin@mail.ru", "newadmin3", "1qa@WS3ed", "Ivan", "Pupkin", null, true);
        String jsonRequest = gson.toJson(request);
        String jsonResponse = server.registerEmployee(jsonRequest);
        LoginUserDtoResponse token = gson.fromJson(jsonResponse, LoginUserDtoResponse.class);
        assertNotNull(token.getToken());

        UserDtoRequest request1 = new UserDtoRequest(token.getToken());
        String jsonRequest1 = gson.toJson(request1);
        String jsonResponse1 = server.deleteAccount(jsonRequest1);
        assertEquals(gson.toJson(new EmptyResponse()), jsonResponse1);
    }

    @Test
    public void deleteUnauthorizedUser() {
        RegisterEmployeeDtoRequest request = new RegisterEmployeeDtoRequest("admin@mail.ru", "newadmin4", "1qa@WS3ed", "Ivan", "Pupkin", null, true);
        String jsonRequest = gson.toJson(request);
        String jsonResponse = server.registerEmployee(jsonRequest);
        LoginUserDtoResponse token = gson.fromJson(jsonResponse, LoginUserDtoResponse.class);
        assertNotNull(token.getToken());

        String status = server.logout(jsonResponse);
        assertEquals(gson.toJson(new EmptyResponse()), status);

        UserDtoRequest request1 = new UserDtoRequest(token.getToken());
        String jsonRequest1 = gson.toJson(request1);
        String jsonResponse1 = server.deleteAccount(jsonRequest1);
        assertEquals(gson.toJson(new ErrorResponse("Неверный токен пользователя")), jsonResponse1);
    }

    @Test
    public void duplicateRegisterEmployee() {
        RegisterEmployeeDtoRequest request = new RegisterEmployeeDtoRequest("admin@mail.ru", "admin", "1qa@WS3ed", "Ivan", "Pupkin", null, true);
        String jsonRequest = gson.toJson(request);
        String jsonResponse = server.registerEmployee(jsonRequest);
        LoginUserDtoResponse token = gson.fromJson(jsonResponse, LoginUserDtoResponse.class);
        assertNotNull(token.getToken());
        jsonResponse = server.registerEmployee(jsonRequest);
        assertEquals(gson.toJson(new ErrorResponse("Пользователь с таким логином уже существует")), jsonResponse);
    }

    @Test
    public void nullFieldsRegisterEmployee() {
        RegisterEmployeeDtoRequest request1 = new RegisterEmployeeDtoRequest("admin@mail.ru", "employeeadmin", "1qa@WS3ed", null, "Pupkin", "Ivanovich", true);
        String jsonRequest1 = gson.toJson(request1);
        String jsonResponse1 = server.registerEmployee(jsonRequest1);
        assertEquals(gson.toJson(new ErrorResponse("Заполните все поля")), jsonResponse1);

        RegisterEmployeeDtoRequest request2 = new RegisterEmployeeDtoRequest("admin@mail.ru", "employeeadmin1", "1qa@WS3ed", "Ivan", null, "Petrovich", true);
        String jsonRequest2 = gson.toJson(request2);
        String jsonResponse2 = server.registerEmployee(jsonRequest2);
        assertEquals(gson.toJson(new ErrorResponse("Заполните все поля")), jsonResponse2);

        RegisterEmployeeDtoRequest request3 = new RegisterEmployeeDtoRequest("admin@mail.ru", "employeeadmin2", null, "Ivan", "Pupkin", "Antonov", true);
        String jsonRequest3 = gson.toJson(request3);
        String jsonResponse3 = server.registerEmployee(jsonRequest3);
        assertEquals(gson.toJson(new ErrorResponse("Заполните все поля")), jsonResponse3);

        RegisterEmployeeDtoRequest request4 = new RegisterEmployeeDtoRequest(null, "employeeadmin3", "1qa@WS3ed", "Ivan", "Pupkin", "Antonov", true);
        String jsonRequest4 = gson.toJson(request4);
        String jsonResponse4 = server.registerEmployee(jsonRequest4);
        assertEquals(gson.toJson(new ErrorResponse("Заполните все поля")), jsonResponse4);

        RegisterEmployeeDtoRequest request5 = new RegisterEmployeeDtoRequest("admin@mail.ru", null, "1qa@WS3ed", "Ivan", "Pupkin", "Antonov", true);
        String jsonRequest5 = gson.toJson(request5);
        String jsonResponse5 = server.registerEmployee(jsonRequest5);
        assertEquals(gson.toJson(new ErrorResponse("Заполните все поля")), jsonResponse5);

        RegisterEmployeeDtoRequest request6 = new RegisterEmployeeDtoRequest("admin@mail.ru", "employeeadmin4", "1qa@WS3ed", "Ivan", "Petrov", null, true);
        String jsonRequest6 = gson.toJson(request6);
        String jsonResponse6 = server.registerEmployee(jsonRequest6);
        LoginUserDtoResponse token = gson.fromJson(jsonResponse6, LoginUserDtoResponse.class);
        assertNotNull(token.getToken());
    }

    @Test
    public void invalidFieldsRegisterEmployee() {
        RegisterEmployeeDtoRequest request1 = new RegisterEmployeeDtoRequest("admin@mail.ru", "employeeadmi5", "1qa@WS3ed", "Iv", "Pupkin", "Egorov", true);
        String jsonRequest1 = gson.toJson(request1);
        String jsonResponse1 = server.registerEmployee(jsonRequest1);
        assertEquals(gson.toJson(new ErrorResponse("Неверное имя")), jsonResponse1);

        RegisterEmployeeDtoRequest request2 = new RegisterEmployeeDtoRequest("admin@mail.ru", "a", "1qa@WS3ed", "Ivan", "Pupkin", "Petrov", true);
        String jsonRequest2 = gson.toJson(request2);
        String jsonResponse2 = server.registerEmployee(jsonRequest2);
        assertEquals(gson.toJson(new ErrorResponse("Неверный логин пользователя")), jsonResponse2);

        RegisterEmployeeDtoRequest request3 = new RegisterEmployeeDtoRequest("admin@mail.ru", "employeeadmin6", "12345", "Ivan", "Pupkin", "Antonov", true);
        String jsonRequest3 = gson.toJson(request3);
        String jsonResponse3 = server.registerEmployee(jsonRequest3);
        assertEquals(gson.toJson(new ErrorResponse("Неверный пароль")), jsonResponse3);

        RegisterEmployeeDtoRequest request4 = new RegisterEmployeeDtoRequest("adminmail.ru", "employeeadmin7", "1qa@WS3ed", "Ivan", "Pupkin", "Egorov", true);
        String jsonRequest4 = gson.toJson(request4);
        String jsonResponse4 = server.registerEmployee(jsonRequest4);
        assertEquals(gson.toJson(new ErrorResponse("Неверный email")), jsonResponse4);

        RegisterEmployeeDtoRequest request5 = new RegisterEmployeeDtoRequest("admin@mail.ru", "employeeadmin8", "1qa@WS3ed", "Ivan", "P", "Antonov", true);
        String jsonRequest5 = gson.toJson(request5);
        String jsonResponse5 = server.registerEmployee(jsonRequest5);
        assertEquals(gson.toJson(new ErrorResponse("Неверная фамилия")), jsonResponse5);

        RegisterEmployeeDtoRequest request6 = new RegisterEmployeeDtoRequest("admin@mail.ru", "employeeadmin9", "1qa@WS3ed", "Ivan", "Petrov", "A", true);
        String jsonRequest6 = gson.toJson(request6);
        String jsonResponse6 = server.registerEmployee(jsonRequest6);
        assertEquals(gson.toJson(new ErrorResponse("Неверное отчество")), jsonResponse6);
    }

    @Test
    public void nullFieldsRegisterEmployer() {
        RegisterEmployerDtoRequest request1 = new RegisterEmployerDtoRequest("admin@mail.ru", "employeradmin", "1qa@WS3ed", null, "Pupkin", "Ivanovich", "aplana", "Omsk, 21-ya Amurskaya, d.74, kv. 14");
        String jsonRequest1 = gson.toJson(request1);
        String jsonResponse1 = server.registerEmployer(jsonRequest1);
        assertEquals(gson.toJson(new ErrorResponse("Заполните все поля")), jsonResponse1);

        RegisterEmployerDtoRequest request2 = new RegisterEmployerDtoRequest("admin@mail.ru", "employeradmin1", "1qa@WS3ed", "Ivan", null, "Petrovich", "aplana", "Omsk, 21-ya Amurskaya, d.74, kv. 14");
        String jsonRequest2 = gson.toJson(request2);
        String jsonResponse2 = server.registerEmployer(jsonRequest2);
        assertEquals(gson.toJson(new ErrorResponse("Заполните все поля")), jsonResponse2);

        RegisterEmployerDtoRequest request3 = new RegisterEmployerDtoRequest("admin@mail.ru", "employeradmin2", null, "Ivan", "Pupkin", "Antonov", "aplana", "Omsk, 21-ya Amurskaya, d.74, kv. 14");
        String jsonRequest3 = gson.toJson(request3);
        String jsonResponse3 = server.registerEmployer(jsonRequest3);
        assertEquals(gson.toJson(new ErrorResponse("Заполните все поля")), jsonResponse3);

        RegisterEmployerDtoRequest request4 = new RegisterEmployerDtoRequest(null, "employeradmin3", "1qa@WS3ed", "Ivan", "Pupkin", "Antonov", "aplana", "Omsk, 21-ya Amurskaya, d.74, kv. 14");
        String jsonRequest4 = gson.toJson(request4);
        String jsonResponse4 = server.registerEmployer(jsonRequest4);
        assertEquals(gson.toJson(new ErrorResponse("Заполните все поля")), jsonResponse4);

        RegisterEmployerDtoRequest request5 = new RegisterEmployerDtoRequest("admin@mail.ru", null, "1qa@WS3ed", "Ivan", "Pupkin", "Antonov", "aplana", "Omsk, 21-ya Amurskaya, d.74, kv. 14");
        String jsonRequest5 = gson.toJson(request5);
        String jsonResponse5 = server.registerEmployer(jsonRequest5);
        assertEquals(gson.toJson(new ErrorResponse("Заполните все поля")), jsonResponse5);

        RegisterEmployerDtoRequest request6 = new RegisterEmployerDtoRequest("admin@mail.ru", "employeradmin4", "1qa@WS3ed", "Ivan", "Pupkin", "Antonov", null, "Omsk, 21-ya Amurskaya, d.74, kv. 14");
        String jsonRequest6 = gson.toJson(request6);
        String jsonResponse6 = server.registerEmployer(jsonRequest6);
        assertEquals(gson.toJson(new ErrorResponse("Заполните все поля")), jsonResponse6);

        RegisterEmployerDtoRequest request7 = new RegisterEmployerDtoRequest("admin@mail.ru", "employeradmin5", "1qa@WS3ed", "Ivan", "Pupkin", "Antonov", "aplana", null);
        String jsonRequest7 = gson.toJson(request7);
        String jsonResponse7 = server.registerEmployer(jsonRequest7);
        assertEquals(gson.toJson(new ErrorResponse("Заполните все поля")), jsonResponse7);

        RegisterEmployerDtoRequest request8 = new RegisterEmployerDtoRequest("admin@mail.ru", "employeradmin6", "1qa@WS3ed", "Ivan", "Pupkin", null, "aplana", "Omsk, 21-ya Amurskaya, d.74, kv. 14");
        String jsonRequest8 = gson.toJson(request8);
        String jsonResponse8 = server.registerEmployer(jsonRequest8);
        LoginUserDtoResponse token = gson.fromJson(jsonResponse8, LoginUserDtoResponse.class);
        assertNotNull(token.getToken());
    }

    @Test
    public void invalidFieldsRegisterEmployer() {
        RegisterEmployerDtoRequest request1 = new RegisterEmployerDtoRequest("admin@mail.ru", "employeradmin7", "1qa@WS3ed", "Iv", "Pupkin", null, "aplana", "Omsk, 21-ya Amurskaya, d.74, kv. 14");
        String jsonRequest1 = gson.toJson(request1);
        String jsonResponse1 = server.registerEmployer(jsonRequest1);
        assertEquals(gson.toJson(new ErrorResponse("Неверное имя")), jsonResponse1);

        RegisterEmployerDtoRequest request2 = new RegisterEmployerDtoRequest("admin@mail.ru", "a", "1qa@WS3ed", "Ivan", "Pupkin", null, "aplana", "Omsk, 21-ya Amurskaya, d.74, kv. 14");
        String jsonRequest2 = gson.toJson(request2);
        String jsonResponse2 = server.registerEmployer(jsonRequest2);
        assertEquals(gson.toJson(new ErrorResponse("Неверный логин пользователя")), jsonResponse2);

        RegisterEmployerDtoRequest request3 = new RegisterEmployerDtoRequest("admin@mail.ru", "employeradmin8", "12345", "Ivan", "Pupkin", "Antonov", "aplana", "Omsk, 21-ya Amurskaya, d.74, kv. 14");
        String jsonRequest3 = gson.toJson(request3);
        String jsonResponse3 = server.registerEmployer(jsonRequest3);
        assertEquals(gson.toJson(new ErrorResponse("Неверный пароль")), jsonResponse3);

        RegisterEmployerDtoRequest request4 = new RegisterEmployerDtoRequest("adminmail.ru", "employeradmin9", "1qa@WS3ed", "Ivan", "Pupkin", "Egorov", "aplana", "Omsk, 21-ya Amurskaya, d.74, kv. 14");
        String jsonRequest4 = gson.toJson(request4);
        String jsonResponse4 = server.registerEmployer(jsonRequest4);
        assertEquals(gson.toJson(new ErrorResponse("Неверный email")), jsonResponse4);

        RegisterEmployerDtoRequest request5 = new RegisterEmployerDtoRequest("admin@mail.ru", "employeradmin10", "1qa@WS3ed", "Ivan", "P", null, "aplana", "Omsk, 21-ya Amurskaya, d.74, kv. 14");
        String jsonRequest5 = gson.toJson(request5);
        String jsonResponse5 = server.registerEmployer(jsonRequest5);
        assertEquals(gson.toJson(new ErrorResponse("Неверная фамилия")), jsonResponse5);

        RegisterEmployerDtoRequest request6 = new RegisterEmployerDtoRequest("admin@mail.ru", "employeradmin11", "1qa@WS3ed", "Ivan", "Petrov", "A", "aplana", "Omsk, 21-ya Amurskaya, d.74, kv. 14");
        String jsonRequest6 = gson.toJson(request6);
        String jsonResponse6 = server.registerEmployer(jsonRequest6);
        assertEquals(gson.toJson(new ErrorResponse("Неверное отчество")), jsonResponse6);
    }

    @Test
    public void successRegisterEmployee() {
        RegisterEmployeeDtoRequest request = new RegisterEmployeeDtoRequest("admin@mail.ru", "aemployee1", "1qa@WS3ed", "Ivan", "Pupkin", null, true);
        String jsonRequest = gson.toJson(request);
        String jsonResponse = server.registerEmployee(jsonRequest);
        LoginUserDtoResponse token = gson.fromJson(jsonResponse, LoginUserDtoResponse.class);
        assertNotNull(token.getToken());
    }

    @Test
    public void successRegisterEmployer() {
        RegisterEmployerDtoRequest request = new RegisterEmployerDtoRequest("admin@mail.ru", "employer1", "1qa@WS3ed", "Ivan", "Petrov", "Anatolievich", "aplana", "Omsk, 21-ya Amurskaya, d.74, kv. 14");
        String jsonRequest = gson.toJson(request);
        String jsonResponse = server.registerEmployer(jsonRequest);
        LoginUserDtoResponse token = gson.fromJson(jsonResponse, LoginUserDtoResponse.class);
        assertNotNull(token.getToken());
    }

    @Test
    public void successEditEmployeeInfo() {
        RegisterEmployeeDtoRequest request = new RegisterEmployeeDtoRequest("admin@mail.ru", "aeditemployee1", "1qa@WS3ed", "Ivan", "Pupkin", null, true);
        String jsonRequest = gson.toJson(request);
        String jsonResponse = server.registerEmployee(jsonRequest);
        LoginUserDtoResponse token = gson.fromJson(jsonResponse, LoginUserDtoResponse.class);
        assertNotNull(token.getToken());

        EditEmployeeDtoRequest request1 = new EditEmployeeDtoRequest(token.getToken(), "newemail@mail.ru", "1qa@WS3ed", "Ivan", "Pupkin", null, true);
        String jsonRequest1 = gson.toJson(request1);
        String jsonResponse1 = server.editEmployeeInfo(jsonRequest1);
        assertEquals(gson.toJson(new EmptyResponse()), jsonResponse1);
    }

    @Test
    public void successEditEmployerInfo() {
        RegisterEmployerDtoRequest request = new RegisterEmployerDtoRequest("admin@mail.ru", "deditemployer1", "1qa@WS3ed", "Ivan", "Petrov", "Anatolievich", "aplana", "Omsk, 21-ya Amurskaya, d.74, kv. 14");
        String jsonRequest = gson.toJson(request);
        String jsonResponse = server.registerEmployer(jsonRequest);
        LoginUserDtoResponse token = gson.fromJson(jsonResponse, LoginUserDtoResponse.class);
        assertNotNull(token.getToken());

        EditEmployerDtoRequest request1 = new EditEmployerDtoRequest("newemail@mail.ru", "1qa@WS3ed", "Ivan", "Petrov", "Anatolievich", "aplana", "Omsk, 21-ya Amurskaya, d.74, kv. 14", token.getToken());
        String jsonRequest1 = gson.toJson(request1);
        String jsonResponse1 = server.editEmployerInfo(jsonRequest1);
        assertEquals(gson.toJson(new EmptyResponse()), jsonResponse1);
    }

    @Test
    public void successAddVacancy() {
        RegisterEmployerDtoRequest request = new RegisterEmployerDtoRequest("admin@mail.ru", "vacancyemployer1", "1qa@WS3ed", "Ivan", "Petrov", "Anatolievich", "aplana", "Omsk, 21-ya Amurskaya, d.74, kv. 14");
        String jsonRequest = gson.toJson(request);
        String jsonResponse = server.registerEmployer(jsonRequest);
        LoginUserDtoResponse token = gson.fromJson(jsonResponse, LoginUserDtoResponse.class);
        assertNotNull(token.getToken());

        requirements.clear();
        requirements.add(new Requirement("Java", 4, true));
        requirements.add(new Requirement("SQL", 4, true));
        requirements.add(new Requirement("JS", 4, false));

        AddVacancyDtoRequest request1 = new AddVacancyDtoRequest(token.getToken(), "Some position", 2500, requirements, true);
        String jsonRequest1 = gson.toJson(request1, AddVacancyDtoRequest.class);
        String jsonResponse1 = server.addVacancy(jsonRequest1);
        assertEquals(gson.toJson(new EmptyResponse()), jsonResponse1);
    }

    @Test
    public void successAddEmployeeSkills() {
        RegisterEmployeeDtoRequest request = new RegisterEmployeeDtoRequest("admin@mail.ru", "eemployee2", "1qa@WS3ed", "Ivan", "Pupkin", null, true);
        String jsonRequest = gson.toJson(request);
        String jsonResponse = server.registerEmployee(jsonRequest);
        LoginUserDtoResponse token = gson.fromJson(jsonResponse, LoginUserDtoResponse.class);
        assertNotNull(token.getToken());

        skills.clear();
        skills.add(new SkillDto("Java", 2));
        skills.add(new SkillDto("SQL", 1));

        AddEmployeeSkillsDtoRequest request1 = new AddEmployeeSkillsDtoRequest(token.getToken(), skills);
        String jsonRequest1 = gson.toJson(request1);
        String jsonResponse1 = server.addSkills(jsonRequest1);
        assertEquals(gson.toJson(new EmptyResponse()), jsonResponse1);
    }

    @AfterClass
    public static void postcondition() {
        server.stopServer(null);
    }
}
