package net.hhkiller.daoimpl;

import net.hhkiller.dao.EmployeeDao;
import net.hhkiller.db.DataBase;
import net.hhkiller.model.Skill;
import net.hhkiller.utils.UserException;

import java.util.Set;

public class EmployeeDaoImpl implements EmployeeDao {
    @Override
    public void addSkills(String token, Set<Skill> skills) throws UserException {
        DataBase.getInstance().insert(token, skills);
    }
}
