package net.hhkiller.daoimpl;

import net.hhkiller.dao.UserDao;
import net.hhkiller.db.DataBase;
import net.hhkiller.model.User;
import net.hhkiller.utils.UserException;

public class UserDaoImpl implements UserDao {
    @Override
    public String insert(User user) throws UserException {
        return DataBase.getInstance().insert(user);
    }

    @Override
    public String login(User user) throws UserException {
        return DataBase.getInstance().login(user);
    }

    @Override
    public void logout(String token) throws UserException {
        DataBase.getInstance().logout(token);
    }

    @Override
    public void delete(String token) throws UserException {
        DataBase.getInstance().delete(token);
    }

    @Override
    public void update(String token, User user) throws UserException {
        DataBase.getInstance().update(token, user);
    }
}
