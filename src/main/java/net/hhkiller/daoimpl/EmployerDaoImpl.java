package net.hhkiller.daoimpl;

import net.hhkiller.dao.EmployerDao;
import net.hhkiller.db.DataBase;
import net.hhkiller.model.Employee;
import net.hhkiller.model.Vacancy;
import net.hhkiller.utils.UserException;

import java.util.Set;

public class EmployerDaoImpl implements EmployerDao {
    @Override
    public void addVacancy(String token, Vacancy vacancy) throws UserException {
        DataBase.getInstance().insert(token, vacancy);
    }

    @Override
    public Set<Employee> searchCandidate(String token, Vacancy vacancy) throws UserException {
        return DataBase.getInstance().searchCandidate(token, vacancy);
    }
}
