package net.hhkiller.server;

import net.hhkiller.service.EmployeeService;
import net.hhkiller.service.EmployerService;
import net.hhkiller.service.UserService;

public class Server {
    private EmployeeService employeeService;
    private EmployerService employerService;
    private UserService userService;


    public Server() {
        employeeService = new EmployeeService();
        employerService = new EmployerService();
        userService = new UserService();
    }

    public void startServer(String savedDataFileName) {
        if (savedDataFileName != null) {
            //TODO load from file
        }
    }

    public void stopServer(String saveDataFileName) {
        if (saveDataFileName != null) {
            //TODO save DataBase to a file
        }
    }

    public String registerEmployee(String requestJsonString) {
        return employeeService.registerEmployee(requestJsonString);
    }

    public String registerEmployer(String requestJsonString) {
        return employerService.registerEmployer(requestJsonString);
    }

    public String login(String requestJsonString) {
        return userService.login(requestJsonString);
    }

    public String logout(String requestJsonString) {
        return userService.logout(requestJsonString);
    }

    public String deleteAccount(String requestJsonString) {
        return userService.deleteAccount(requestJsonString);
    }

    public String editEmployeeInfo(String requestJsonString) {
        return employeeService.editEmployeeInfo(requestJsonString);
    }

    public String editEmployerInfo(String requestJsonString) {
        return employerService.editEmployerInfo(requestJsonString);
    }

    public String addVacancy(String requestJsonString) {
        return employerService.addVacancy(requestJsonString);
    }

    public String searchCandidate(String requestJsonString){
        return employerService.searchCandidate(requestJsonString);
    }

    public String editVacancy(String requestJsonString) {
        return null;
    }

    public String deleteVacancy(String requestJsonString) {
        return null;
    }

    public String disableVacancy(String requestJsonString) {
        return null;
    }

    public String enableVacancy(String requestJsonString) {
        return null;
    }

    public String showVacancies(String requestJsonString) {
        return null;
    }

    public String addSkills(String requestJsonString) {
        return employeeService.addSkills(requestJsonString);
    }

    public String editSkills(String requestJsonString) {
        return null;
    }

    public String disableProfile(String requestJsonString) {
        return null;
    }

    public String enableProfile(String requestJsonString) {
        return null;
    }

    public String hireCandidate(String requestJsonString) {
        return null;
    }
}
