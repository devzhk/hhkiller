package net.hhkiller.request;

import net.hhkiller.utils.UserErrorCode;
import net.hhkiller.utils.UserException;

public class RegisterEmployeeDtoRequest extends LoginUserDtoRequest {
    private String firstName;
    private String lastName;
    private String patronymic;
    private String email;
    private boolean isActive;

    public RegisterEmployeeDtoRequest(String email, String login, String password, String firstName, String lastName, String patronymic, boolean isActive) {
        super(login, password);
        this.firstName = firstName;
        this.lastName = lastName;
        this.patronymic = patronymic;
        this.email = email;
        this.isActive = isActive;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    @Override
    public void validate() throws UserException {
        super.validate();
        if (firstName == null || lastName == null || email == null) {
            throw new UserException(UserErrorCode.USER_FILL_ALL_FIELDS);
        }
        if (!firstName.matches("^[a-zA-Z]{3,14}$")) {
            throw new UserException(UserErrorCode.USER_WRONG_FIRST_NAME);
        }
        if (!lastName.matches("^[a-zA-Z]{3,14}$")) {
            throw new UserException(UserErrorCode.USER_WRONG_LAST_NAME);
        }
        if (!email.matches("^[a-zA-Z0-9_!#$%&’*+/=?`{|}~^.-]+@[a-zA-Z0-9.-]+$")) {
            throw new UserException(UserErrorCode.USER_WRONG_EMAIL);
        }
        if (patronymic != null) {
            if (!patronymic.matches("^[a-zA-Z]{3,14}$")) {
                throw new UserException(UserErrorCode.USER_WRONG_PATRONYMIC);
            }
        }
    }
}
