package net.hhkiller.request;

import net.hhkiller.utils.UserErrorCode;
import net.hhkiller.utils.UserException;

public class EditEmployerDtoRequest extends RegisterEmployerDtoRequest {
    private String token;

    public EditEmployerDtoRequest(String email, String password, String contactFirstName, String contactLastName, String contactPatronymic, String companyName, String address, String token) {
        super(email, "emptylogin", password, contactFirstName, contactLastName, contactPatronymic, companyName, address);
        this.token = token;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @Override
    public void validate() throws UserException {
        super.validate();
        if (token == null){
            throw new UserException(UserErrorCode.USER_TOKEN_ERROR);
        }
    }
}
