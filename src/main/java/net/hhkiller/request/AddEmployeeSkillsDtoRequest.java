package net.hhkiller.request;

import net.hhkiller.utils.UserErrorCode;
import net.hhkiller.utils.UserException;

import java.util.Set;

public class AddEmployeeSkillsDtoRequest {
    private String token;
    private Set<SkillDto> skills;

    public AddEmployeeSkillsDtoRequest(String token, Set<SkillDto> skills) {
        this.token = token;
        this.skills = skills;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Set<SkillDto> getSkills() {
        return skills;
    }

    public void setSkills(Set<SkillDto> skills) {
        this.skills = skills;
    }

    public void validate() throws UserException {
        if (token == null) {
            throw new UserException(UserErrorCode.USER_TOKEN_ERROR);
        }
        if (skills == null) {
            throw new UserException(UserErrorCode.USER_FILL_ALL_FIELDS);
        }
        for (SkillDto skillDto : skills) {
            skillDto.validate();
        }
    }
}
