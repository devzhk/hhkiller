package net.hhkiller.request;

import net.hhkiller.utils.UserErrorCode;
import net.hhkiller.utils.UserException;

public class UserDtoRequest {
    private String token;

    public UserDtoRequest(String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public void validate() throws UserException {
        if (token == null) {
            throw new UserException(UserErrorCode.USER_TOKEN_ERROR);
        }
    }
}
