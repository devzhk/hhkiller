package net.hhkiller.request;

import net.hhkiller.model.Requirement;
import net.hhkiller.utils.UserErrorCode;
import net.hhkiller.utils.UserException;

import java.util.Set;

public class AddVacancyDtoRequest {
    private String token;
    private String position;
    private int salary;
    private Set<Requirement> requirements;
    private boolean isActive;

    public AddVacancyDtoRequest(String token, String position, int salary, Set<Requirement> requirements, boolean isActive) {
        this.token = token;
        this.position = position;
        this.salary = salary;
        this.requirements = requirements;
        this.isActive = isActive;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public Set<Requirement> getRequirements() {
        return requirements;
    }

    public void setRequirements(Set<Requirement> requirements) {
        this.requirements = requirements;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public void validate() throws UserException {
        if (token == null || position == null || salary == 0 || requirements == null) {
            throw new UserException(UserErrorCode.USER_FILL_ALL_FIELDS);
        }
        for (Requirement requirement : requirements) {
            if (requirement.getLevel() < 1 || requirement.getLevel() > 5) {
                throw new UserException(UserErrorCode.USER_SKILL_LEVEL_ERROR);
            }
        }
    }
}
