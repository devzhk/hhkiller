package net.hhkiller.request;

import net.hhkiller.utils.UserErrorCode;
import net.hhkiller.utils.UserException;

public class EditEmployeeDtoRequest extends RegisterEmployeeDtoRequest {
    private String token;

    public EditEmployeeDtoRequest(String token, String email, String password, String firstName, String lastName, String patronymic, boolean isActive) {
        super(email, "emptylogin", password, firstName, lastName, patronymic, isActive);
        this.token = token;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @Override
    public void validate() throws UserException {
        super.validate();
        if (token == null){
            throw new UserException(UserErrorCode.USER_TOKEN_ERROR);
        }
    }
}
