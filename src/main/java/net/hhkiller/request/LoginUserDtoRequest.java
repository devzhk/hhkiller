package net.hhkiller.request;

import net.hhkiller.utils.UserErrorCode;
import net.hhkiller.utils.UserException;

public class LoginUserDtoRequest {
    private String login;
    private String password;

    public LoginUserDtoRequest(String login, String password) {
        this.login = login;
        this.password = password;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void validate() throws UserException {
        if (login == null || password == null) {
            throw new UserException(UserErrorCode.USER_FILL_ALL_FIELDS);
        }
        if (!password.matches("^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,}$")) {
            throw new UserException(UserErrorCode.USER_WRONG_PASSWORD);
        }
        if (!login.matches("^[a-zA-Z0-9._-]{3,16}$")) {
            throw new UserException(UserErrorCode.USER_WRONG_LOGIN);
        }
    }
}
