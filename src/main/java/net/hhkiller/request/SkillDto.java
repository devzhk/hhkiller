package net.hhkiller.request;

import net.hhkiller.utils.UserErrorCode;
import net.hhkiller.utils.UserException;

public class SkillDto {
    private String name;
    private int level;

    public SkillDto(String name, int level) {
        this.name = name;
        this.level = level;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public void validate() throws UserException {
        if (name == null || name.equals("")) {
            throw new UserException(UserErrorCode.USER_FILL_ALL_FIELDS);
        }
        if (level < 1 || level > 5) {
            throw new UserException(UserErrorCode.USER_SKILL_LEVEL_ERROR);
        }
    }
}
