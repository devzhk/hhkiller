package net.hhkiller.request;

import net.hhkiller.utils.UserErrorCode;
import net.hhkiller.utils.UserException;

public class RegisterEmployerDtoRequest extends LoginUserDtoRequest {
    private String contactFirstName;
    private String contactLastName;
    private String contactPatronymic;
    private String companyName;
    private String address;
    private String email;

    public RegisterEmployerDtoRequest(String email, String login, String password, String contactFirstName,
                                      String contactLastName, String contactPatronymic, String companyName, String address) {
        super(login, password);
        this.contactFirstName = contactFirstName;
        this.contactLastName = contactLastName;
        this.contactPatronymic = contactPatronymic;
        this.companyName = companyName;
        this.address = address;
        this.email = email;
    }

    public String getContactFirstName() {
        return contactFirstName;
    }

    public void setContactFirstName(String contactFirstName) {
        this.contactFirstName = contactFirstName;
    }

    public String getContactLastName() {
        return contactLastName;
    }

    public void setContactLastName(String contactLastName) {
        this.contactLastName = contactLastName;
    }

    public String getContactPatronymic() {
        return contactPatronymic;
    }

    public void setContactPatronymic(String contactPatronymic) {
        this.contactPatronymic = contactPatronymic;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public void validate() throws UserException {
        super.validate();
        if (companyName == null || address == null || contactFirstName == null
                || contactLastName == null || email == null) {
            throw new UserException(UserErrorCode.USER_FILL_ALL_FIELDS);
        }
        if (!companyName.matches("^[a-zA-Z0-9._-]{3,14}$")) {
            throw new UserException(UserErrorCode.USER_WRONG_COMPANY_NAME);
        }
        if (!contactFirstName.matches("^[a-zA-Z]{3,14}$")) {
            throw new UserException(UserErrorCode.USER_WRONG_FIRST_NAME);
        }
        if (!contactLastName.matches("^[a-zA-Z]{3,14}$")) {
            throw new UserException(UserErrorCode.USER_WRONG_LAST_NAME);
        }
        if (!address.matches("^[ a-zA-Z0-9.,-]{5,50}$")) {
            throw new UserException(UserErrorCode.USER_WRONG_ADDRESS);
        }
        if (!email.matches("^[a-zA-Z0-9_!#$%&’*+/=?`{|}~^.-]+@[a-zA-Z0-9.-]+$")) {
            throw new UserException(UserErrorCode.USER_WRONG_EMAIL);
        }
        if (contactPatronymic != null) {
            if (!contactPatronymic.matches("^[a-zA-Z]{3,14}$")) {
                throw new UserException(UserErrorCode.USER_WRONG_PATRONYMIC);
            }
        }
    }
}
