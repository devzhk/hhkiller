package net.hhkiller.utils;

public enum UserErrorCode {
    USER_WRONG_LOGIN("Неверный логин пользователя"),
    USER_WRONG_PASSWORD("Неверный пароль"),
    USER_WRONG_LOGIN_OR_PASSWORD("Неверное имя пользователя или пароль"),
    USER_WRONG_FIRST_NAME("Неверное имя"),
    USER_WRONG_LAST_NAME("Неверная фамилия"),
    USER_WRONG_PATRONYMIC("Неверное отчество"),
    USER_WRONG_EMAIL("Неверный email"),
    USER_FILL_ALL_FIELDS("Заполните все поля"),
    USER_WRONG_ADDRESS("Неверный адрес"),
    USER_TOKEN_ERROR("Неверный токен пользователя"),
    USER_WRONG_COMPANY_NAME("Неверное название компании"),
    USER_ALREADY_LOGGED("Пользователь уже залогинен"),
    USER_SKILL_LEVEL_ERROR("Уровень владения навыком должен быть в диапозоне 1-5"),
    USER_ALREADY_EXIST("Пользователь с таким логином уже существует"),
    VACANCY_NOT_FOUND("Данная ваканси не найдена");

    private String error;

    UserErrorCode(String errorMessage) {
        this.error = errorMessage;
    }

    public String getError() {
        return error;
    }
}
