package net.hhkiller.utils;

public class UserException extends Exception {
    private UserErrorCode userErrorCode;

    public UserException(UserErrorCode userErrorCode) {
        this.userErrorCode = userErrorCode;
    }

    public UserErrorCode getErrorCode() {
        return userErrorCode;
    }
}
