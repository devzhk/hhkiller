package net.hhkiller.service;

import com.google.gson.Gson;
import net.hhkiller.response.EmptyResponse;
import net.hhkiller.response.ErrorResponse;
import net.hhkiller.response.LoginUserDtoResponse;
import net.hhkiller.dao.EmployerDao;
import net.hhkiller.dao.UserDao;
import net.hhkiller.daoimpl.EmployerDaoImpl;
import net.hhkiller.daoimpl.UserDaoImpl;
import net.hhkiller.model.Employee;
import net.hhkiller.model.Employer;
import net.hhkiller.model.Vacancy;
import net.hhkiller.request.AddVacancyDtoRequest;
import net.hhkiller.request.EditEmployerDtoRequest;
import net.hhkiller.request.RegisterEmployerDtoRequest;
import net.hhkiller.utils.UserException;

import java.util.Set;

public class EmployerService {
    private Gson gson;
    private UserDao userDao;
    private EmployerDao employerDao;

    public EmployerService() {
        this.gson = new Gson();
        this.userDao = new UserDaoImpl();
        this.employerDao = new EmployerDaoImpl();
    }

    public String registerEmployer(String requestJsonString) {
        try {
            RegisterEmployerDtoRequest request = gson.fromJson(requestJsonString, RegisterEmployerDtoRequest.class);
            request.validate();
            String token = userDao.insert(new Employer(request.getEmail(), request.getLogin(), request.getPassword(), request.getContactFirstName(), request.getContactLastName(), request.getContactPatronymic(), request.getCompanyName(), request.getAddress()));
            return gson.toJson(new LoginUserDtoResponse(token));
        } catch (UserException e) {
            return gson.toJson(new ErrorResponse(e.getErrorCode().getError()));
        }
    }

    public String editEmployerInfo(String requestJsonString) {
        try {
            EditEmployerDtoRequest request = gson.fromJson(requestJsonString, EditEmployerDtoRequest.class);
            request.validate();
            userDao.update(request.getToken(), new Employer(request.getEmail(), request.getLogin(), request.getPassword(), request.getContactFirstName(), request.getContactLastName(), request.getContactPatronymic(), request.getCompanyName(), request.getAddress()));
            return gson.toJson(new EmptyResponse());
        } catch (UserException e) {
            return gson.toJson(new ErrorResponse(e.getErrorCode().getError()));
        }
    }

    public String addVacancy(String requestJsonString) {
        try {
            AddVacancyDtoRequest request = gson.fromJson(requestJsonString, AddVacancyDtoRequest.class);
            request.validate();
            employerDao.addVacancy(request.getToken(), new Vacancy(request.getPosition(), request.getSalary(), request.getRequirements(), request.isActive()));
            return gson.toJson(new EmptyResponse());
        } catch (UserException e) {
            return gson.toJson(new ErrorResponse(e.getErrorCode().getError()));
        }
    }

    public String searchCandidate(String requestJsonString) {
        try {
            AddVacancyDtoRequest request = gson.fromJson(requestJsonString, AddVacancyDtoRequest.class);
            request.validate();
            Set<Employee> employees = employerDao.searchCandidate(request.getToken(), new Vacancy(request.getPosition(), request.getSalary(), request.getRequirements(), request.isActive()));
            return gson.toJson(employees);
        } catch (UserException e) {
            return gson.toJson(new ErrorResponse(e.getErrorCode().getError()));
        }
    }
}
