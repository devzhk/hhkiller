package net.hhkiller.service;

import com.google.gson.Gson;
import net.hhkiller.response.EmptyResponse;
import net.hhkiller.response.ErrorResponse;
import net.hhkiller.response.LoginUserDtoResponse;
import net.hhkiller.dao.EmployeeDao;
import net.hhkiller.dao.UserDao;
import net.hhkiller.daoimpl.EmployeeDaoImpl;
import net.hhkiller.daoimpl.UserDaoImpl;
import net.hhkiller.model.Employee;
import net.hhkiller.model.Skill;
import net.hhkiller.request.AddEmployeeSkillsDtoRequest;
import net.hhkiller.request.EditEmployeeDtoRequest;
import net.hhkiller.request.RegisterEmployeeDtoRequest;
import net.hhkiller.request.SkillDto;
import net.hhkiller.utils.UserException;

import java.util.HashSet;
import java.util.Set;

public class EmployeeService {
    private Gson gson;
    private UserDao userDao;
    private EmployeeDao employeeDao;

    public EmployeeService() {
        this.gson = new Gson();
        this.userDao = new UserDaoImpl();
        this.employeeDao = new EmployeeDaoImpl();
    }

    public String registerEmployee(String requestJsonString) {
        try {
            RegisterEmployeeDtoRequest request = gson.fromJson(requestJsonString, RegisterEmployeeDtoRequest.class);
            request.validate();
            String token = userDao.insert(new Employee(request.getEmail(), request.getLogin(), request.getPassword(), request.getFirstName(), request.getLastName(), request.getPatronymic(), request.isActive()));
            return gson.toJson(new LoginUserDtoResponse(token));
        } catch (UserException e) {
            return gson.toJson(new ErrorResponse(e.getErrorCode().getError()));
        }
    }

    public String editEmployeeInfo(String requestJsonString) {
        try {
            EditEmployeeDtoRequest request = gson.fromJson(requestJsonString, EditEmployeeDtoRequest.class);
            request.validate();
            userDao.update(request.getToken(), new Employee(request.getEmail(), request.getLogin(), request.getPassword(), request.getFirstName(), request.getLastName(), request.getPatronymic(), request.isActive()));
            return gson.toJson(new EmptyResponse());
        } catch (UserException e) {
            return gson.toJson(new ErrorResponse(e.getErrorCode().getError()));
        }
    }

    public String addSkills(String requestJsonString) {
        try {
            AddEmployeeSkillsDtoRequest request = gson.fromJson(requestJsonString, AddEmployeeSkillsDtoRequest.class);
            request.validate();
            Set<Skill> skills = new HashSet<>();
            Set<SkillDto> skillDtos = request.getSkills();
            for (SkillDto skillDto : skillDtos) {
                skills.add(new Skill(skillDto.getName(), skillDto.getLevel()));
            }
            employeeDao.addSkills(request.getToken(), skills);
            return gson.toJson(new EmptyResponse());
        } catch (UserException e) {
            return gson.toJson(new ErrorResponse(e.getErrorCode().getError()));
        }
    }
}
