package net.hhkiller.service;

import com.google.gson.Gson;
import net.hhkiller.response.EmptyResponse;
import net.hhkiller.response.ErrorResponse;
import net.hhkiller.response.LoginUserDtoResponse;
import net.hhkiller.dao.UserDao;
import net.hhkiller.daoimpl.UserDaoImpl;
import net.hhkiller.model.User;
import net.hhkiller.request.LoginUserDtoRequest;
import net.hhkiller.request.UserDtoRequest;
import net.hhkiller.utils.UserException;

public class UserService {
    private Gson gson;
    private UserDao userDao;

    public UserService() {
        this.gson = new Gson();
        this.userDao = new UserDaoImpl();
    }

    public String login(String requestJsonString) {
        try {
            LoginUserDtoRequest request = gson.fromJson(requestJsonString, LoginUserDtoRequest.class);
            request.validate();
            String token = userDao.login(new User(request.getLogin(), request.getPassword()));
            return gson.toJson(new LoginUserDtoResponse(token));
        } catch (UserException e) {
            return gson.toJson(new ErrorResponse(e.getErrorCode().getError()));
        }
    }

    public String logout(String requestJsonString) {
        try {
            UserDtoRequest request = gson.fromJson(requestJsonString, UserDtoRequest.class);
            request.validate();
            userDao.logout(request.getToken());
            return gson.toJson(new EmptyResponse());
        } catch (UserException e) {
            return gson.toJson(new ErrorResponse(e.getErrorCode().getError()));
        }
    }

    public String deleteAccount(String requestJsonString) {
        try {
            UserDtoRequest request = gson.fromJson(requestJsonString, UserDtoRequest.class);
            request.validate();
            userDao.delete(request.getToken());
            return gson.toJson(new EmptyResponse());
        } catch (UserException e) {
            return gson.toJson(new ErrorResponse(e.getErrorCode().getError()));
        }
    }
}
