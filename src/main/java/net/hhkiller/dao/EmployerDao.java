package net.hhkiller.dao;

import net.hhkiller.model.Employee;
import net.hhkiller.model.Vacancy;
import net.hhkiller.utils.UserException;

import java.util.Set;

public interface EmployerDao {
    void addVacancy(String token, Vacancy vacancy) throws UserException;
    Set<Employee> searchCandidate(String token, Vacancy vacancy) throws UserException;
}
