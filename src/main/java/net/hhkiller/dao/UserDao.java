package net.hhkiller.dao;

import net.hhkiller.model.User;
import net.hhkiller.utils.UserException;

public interface UserDao {
    String insert(User user) throws UserException;
    String login(User user) throws UserException;
    void logout(String token) throws UserException;
    void delete(String token) throws UserException;
    void update(String token, User user) throws UserException;
}
