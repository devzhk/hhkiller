package net.hhkiller.dao;

import net.hhkiller.model.Skill;
import net.hhkiller.utils.UserException;

import java.util.Set;

public interface EmployeeDao{
    void addSkills(String token, Set<Skill> skills) throws UserException;
}
