package net.hhkiller.model;

import java.util.Objects;
import java.util.Set;

public class Vacancy {
    private String position;
    private int salary;
    private Set<Requirement> requirements;
    private boolean isActive;

    public Vacancy(String position, int salary, Set<Requirement> requirements, boolean isActive) {
        this.position = position;
        this.salary = salary;
        this.requirements = requirements;
        this.isActive = isActive;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public Set<Requirement> getRequirements() {
        return requirements;
    }

    public void setRequirements(Set<Requirement> requirements) {
        this.requirements = requirements;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Vacancy vacancy = (Vacancy) o;
        return salary == vacancy.salary &&
                isActive == vacancy.isActive &&
                Objects.equals(position, vacancy.position) &&
                Objects.equals(requirements, vacancy.requirements);
    }

    @Override
    public int hashCode() {
        return Objects.hash(position, salary, requirements, isActive);
    }
}
