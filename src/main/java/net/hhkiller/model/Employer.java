package net.hhkiller.model;

import java.util.Objects;

public class Employer extends User {
    private String contactFirstName;
    private String contactLastName;
    private String contactPatronymic;
    private String companyName;
    private String address;
    private String email;

    public Employer(String email, String login, String password, String contactFirstName, String contactLastName,
                    String contactPatronymic, String companyName, String address) {
        super(login, password);
        this.contactFirstName = contactFirstName;
        this.contactLastName = contactLastName;
        this.contactPatronymic = contactPatronymic;
        this.companyName = companyName;
        this.address = address;
        this.email = email;
    }

    public String getContactFirstName() {
        return contactFirstName;
    }

    public void setContactFirstName(String contactFirstName) {
        this.contactFirstName = contactFirstName;
    }

    public String getContactLastName() {
        return contactLastName;
    }

    public void setContactLastName(String contactLastName) {
        this.contactLastName = contactLastName;
    }

    public String getContactPatronymic() {
        return contactPatronymic;
    }

    public void setContactPatronymic(String contactPatronymic) {
        this.contactPatronymic = contactPatronymic;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Employer employer = (Employer) o;
        return Objects.equals(contactFirstName, employer.contactFirstName) &&
                Objects.equals(contactLastName, employer.contactLastName) &&
                Objects.equals(contactPatronymic, employer.contactPatronymic) &&
                Objects.equals(companyName, employer.companyName) &&
                Objects.equals(address, employer.address) &&
                Objects.equals(email, employer.email);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), contactFirstName, contactLastName, contactPatronymic, companyName, address, email);
    }
}
