package net.hhkiller.model;

import java.util.Objects;

public class Requirement extends Skill {
    private boolean isNecessary;

    public Requirement(String name, int level, boolean isNecessary) {
        super(name, level);
        this.isNecessary = isNecessary;
    }

    public boolean isNecessary() {
        return isNecessary;
    }

    public void setNecessary(boolean necessary) {
        isNecessary = necessary;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Requirement that = (Requirement) o;
        return isNecessary == that.isNecessary;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), isNecessary);
    }
}
