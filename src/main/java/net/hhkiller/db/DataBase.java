package net.hhkiller.db;

import net.hhkiller.model.Employee;
import net.hhkiller.model.Skill;
import net.hhkiller.model.User;
import net.hhkiller.model.Vacancy;
import net.hhkiller.utils.UserErrorCode;
import net.hhkiller.utils.UserException;
import org.apache.commons.collections4.BidiMap;
import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.collections4.bidimap.DualHashBidiMap;
import org.apache.commons.collections4.multimap.HashSetValuedHashMap;

import java.util.*;

public class DataBase {
    private static DataBase dataBase = null;
    private Map<String, User> registeredUsers;
    private BidiMap<String, String> loggedUsers;
    private Set<Skill> skillsLibrary;
    private MultiValuedMap<String, Vacancy> inactiveVacancies;
    private MultiValuedMap<String, Vacancy> activeVacancies;
    private MultiValuedMap<String, Employee> skills;

    private DataBase() {
        this.registeredUsers = new HashMap<>();
        this.loggedUsers = new DualHashBidiMap<>();
        this.activeVacancies = new HashSetValuedHashMap<>();
        this.inactiveVacancies = new HashSetValuedHashMap<>();
        this.skillsLibrary = new TreeSet<>(Comparator.comparing(Skill::getName));
        this.skillsLibrary.add(new Skill("Java", 0));
        this.skillsLibrary.add(new Skill("SQL", 0));
        this.skills = new HashSetValuedHashMap<>();
    }

    public static DataBase getInstance() {
        if (dataBase == null) {
            dataBase = new DataBase();
        }
        return dataBase;
    }

    public String insert(User user) throws UserException {
        if (registeredUsers.putIfAbsent(user.getLogin(), user) != null) {
            throw new UserException(UserErrorCode.USER_ALREADY_EXIST);
        }
        return login(user);
    }

    public String login(User user) throws UserException {
        if (loggedUsers.containsValue(user.getLogin())) {
            throw new UserException(UserErrorCode.USER_ALREADY_LOGGED);
        }
        if (registeredUsers.get(user.getLogin()) == null || !registeredUsers.get(user.getLogin()).getPassword().equals(user.getPassword())) {
            throw new UserException(UserErrorCode.USER_WRONG_LOGIN_OR_PASSWORD);
        }
        String token = UUID.randomUUID().toString();
        loggedUsers.put(token, user.getLogin());
        return token;
    }

    public void logout(String token) throws UserException {
        if (loggedUsers.remove(token) == null) {
            throw new UserException(UserErrorCode.USER_TOKEN_ERROR);
        }
    }

    public void delete(String token) throws UserException {
        String login = getLoginByToken(token);
        if (registeredUsers.remove(login) == null || loggedUsers.remove(token) == null) {
            throw new UserException(UserErrorCode.USER_WRONG_LOGIN);
        }
    }

    public void update(String token, User user) throws UserException {
        user.setLogin(getLoginByToken(token));
        if (registeredUsers.put(user.getLogin(), user) == null) {
            throw new UserException(UserErrorCode.USER_WRONG_LOGIN);
        }
    }

    public void insert(String token, Vacancy vacancy) throws UserException {
        String login = getLoginByToken(token);
        skillsLibrary.addAll(vacancy.getRequirements());
        if (vacancy.isActive()) {
            activeVacancies.put(login, vacancy);
        } else {
            inactiveVacancies.put(login, vacancy);
        }
    }

    public void insert(String token, Set<Skill> skills) throws UserException {
        String login = getLoginByToken(token);
        Employee employee = (Employee) registeredUsers.get(login);
        employee.setSkills(skills);
        for (Skill skill : skills) {
            this.skills.put(skill.getName(), employee);
        }
        registeredUsers.replace(employee.getLogin(), employee);
    }

    public Set<Employee> searchCandidate(String token, Vacancy vacancy) throws UserException {
        getLoginByToken(token);
        if (!activeVacancies.containsValue(vacancy) && !inactiveVacancies.containsValue(vacancy)){
            throw new UserException(UserErrorCode.VACANCY_NOT_FOUND);
        }
        Set<Employee> employees = new HashSet<>();

        return null;
    }

    public Map<String, User> getRegisteredUsers() {
        return registeredUsers;
    }

    public void setRegisteredUsers(Map<String, User> registeredUsers) {
        this.registeredUsers = registeredUsers;
    }

    public BidiMap<String, String> getLoggedUsers() {
        return loggedUsers;
    }

    public void setLoggedUsers(BidiMap<String, String> loggedUsers) {
        this.loggedUsers = loggedUsers;
    }

    public Set<Skill> getSkillsLibrary() {
        return skillsLibrary;
    }

    public void setSkillsLibrary(Set<Skill> skillsLibrary) {
        this.skillsLibrary = skillsLibrary;
    }

    public MultiValuedMap<String, Vacancy> getInactiveVacancies() {
        return inactiveVacancies;
    }

    public void setInactiveVacancies(MultiValuedMap<String, Vacancy> inactiveVacancies) {
        this.inactiveVacancies = inactiveVacancies;
    }

    public MultiValuedMap<String, Vacancy> getActiveVacancies() {
        return activeVacancies;
    }

    public void setActiveVacancies(MultiValuedMap<String, Vacancy> activeVacancies) {
        this.activeVacancies = activeVacancies;
    }

    public MultiValuedMap<String, Employee> getSkills() {
        return skills;
    }

    public void setSkills(MultiValuedMap<String, Employee> skills) {
        this.skills = skills;
    }

    private String getLoginByToken(String token) throws UserException {
        String login = loggedUsers.get(token);
        if (login == null) {
            throw new UserException(UserErrorCode.USER_TOKEN_ERROR);
        }
        return login;
    }

    private User getUserByToken(String token) throws UserException {
        return registeredUsers.get(getLoginByToken(token));
    }
}
